<!DOCTYPE html>
<html>
<head>
  <title>Receive Voucher</title>
    <style>
      h1 
      {
        background: #000;
        border-radius: 0.25em;
        color: #FFF;
        margin: 0 0 1em;
        padding: 0.5em 0;
        font: bold 100% sans-serif;
        letter-spacing: 0.5em;
        text-align: center;
        text-transform: uppercase;
      }
      
      table { font-size: 85%; table-layout: fixed; width: 100%; }
      table { border-collapse: separate; border-spacing: 2px; }
      th, td { border-width: 1px; padding: 0.5em; position: relative; text-align: left; }
      th, td { border-radius: 0.25em; border-style: solid; }
      th { background: #EEE; border-color: #BBB; }
      td { border-color: #DDD; }
      table.meta:after, table.balance:after { clear: both; content: ""; display: table; }
      table.meta2:after, table.balance:after { clear: both; content: ""; display: table; }

      .divLeft {
        width:450px;
        display:block;
        float: left;
      }
      .divRight {
        width:450px;
        display:block;
        float: right;
      }

    table.inventory { clear: both; width: 100%; }
    table.inventory th { font-weight: bold; text-align: center; }
    table.inventory td:nth-child(1) { text-align: center; width: 26%; }
    table.inventory td:nth-child(2) { text-align: center; width: 38%; }
    table.inventory td:nth-child(3) { text-align: center; width: 12%; }
    table.inventory td:nth-child(4) { text-align: center; width: 12%; }
    table.inventory td:nth-child(5) { text-align: center; width: 12%; }
    h3 { border: none; border-width: 0 0 1px; margin: 0 0 -1em; }
    h3 { border-color: #999; border-bottom-style: solid; }

    .footer {
         position: fixed;
         left: 0;
         bottom: 0;
         width: 100%;
      }

     .footer p {text-align: center;} 
      
    </style>
</head>

<body>
  <h1>Receive Voucher</h1><br>
  <div class="divLeft">
    <table class="meta">
      <tr>
        <th><span>Serial No. #</span></th>
        <td>{{ $sl++ }} </td>
      </tr>
      <tr>
        <th><span>From Account</span></th>
        {{-- <td>{{ $receive->getCust->getCust->title }}</td> --}}
      </tr>
      <tr>
        <th><span>Bill Date</span></th>
        <td>{{ $receive->bill_date }}</td>
      </tr>
    </table>
  </div>

  <div class="divRight">
    <table class="meta2">
      <tr>
        <th><span>Purchase No. #</span></th>
        <td>{{ $receive->order_number }}</td>
      </tr>
      <tr>
        <th><span>To Account</span></th>
        <td>{{ $receive->getSupp->supplier }}</td>
      </tr>
      <tr>
        <th><span>Due Date</span></th>
        <td>{{ $receive->due_date }}</td>
      </tr>
    </table><br><br>
  </div>

  <table class="inventory">
    <thead>
      <tr>
        <th>Item</th>
        <th>Description</th>
        <th>Rate</th>
        <th>Quantity</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{ $receive->item_name }}</td>
        <td>{{ $receive->desc }}</td>
        <td>{{ $receive->rate }}</td>
        <td>{{ $receive->quantity }}</td>
        <td>{{ $receive->quantity*$receive->rate }}</td>
      </tr>
    </tbody>
  </table>

  <div class="footer">
    <h3>Created By : {{ $receive->getUser->name }} </h3>
      <p>{{ $setting->copyright }} </p>
    </div>
</body>

</html>