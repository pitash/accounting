@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
          <a target="_blank" href="" class="btn btn-primary btn-sm"><i
              class="fa fa-folder-open"  ></i>&nbsp; Print Into PDF
          </a>
        </div>
     </div>
   </div>
    <div class="panel-body page-break">
      <div class="row align-items-center">
        <div class="col-sm-5 col-md-offset-4" style="text-align:center;">
          <div style="text-align:center;">
          <img style="margin-left: 97px;" src="{{asset('public/CompLogo/'.$set->getCompany->comp_logo)}}" class="img-responsive" />
          <h4 class="m-n">{{ $set->getCompany->company }}</h4>
          <h4 class="m-n">{{ $set->getCompany->address }}</h4>
          <h2 class="m-n">Group Summary</h2>
          <p class="m-n">{{ date('d-m-Y', strtotime($from)) }} to {{ date('d-m-Y', strtotime($to)) }}</p>
          <br>
        </div>
     </div>

     <div class="panel-body">
      <table id="pattern-type-data" class="table">
        <thead style="background-color:#b3ecff" >
          <tr>
            <th class="">
              <div class="col-md-3">Particulars</div>
              <div class="col-md-3">Amount</div>
              <div class="col-md-3">Debit</div>
              <div class="col-md-3">Creadit</div>
            </th>
          {{-- <th class="">Particulars </th>
          <th class="">Initial Amount</th> --}}
          {{-- <th class="">Debit</th>
          <th class="">Creadit</th> --}}
        </tr>
        </thead>
        <tbody>
          {{-- @forelse ($banks as $banks)
          <tr>
            <td>{{ $banks->bank_name }}</td>
            <td>{{ $banks->bank_name }}</td>
            <td>{{ $banks->initial_amount }}</td>
            <td>{{ $banks->initial_amount }}</td>
          </tr> 
          @empty
              <div class="col-md-6">No Bank Here This Date Range</div>
          @endforelse --}}

          <tr>
            <td><div class="col-md-3"><b>Bank Account</b></div><br>
              @forelse ($banks as $banks)
              <div class="col-md-3">{{ $banks->bank_name }}</div>
              <div class="col-md-3">{{ $banks->initial_amount }}</div>
              <div class="col-md-3">{{ $banks->initial_amount }}</div>
              <div class="col-md-3">&nbsp;</div>
              @empty
              <div class="col-md-6">No Bank Account Here This Date Range</div>
              @endforelse
            </td>
          </tr>

          <tr>
            <td><div class="col-md-3"><b>Sundry Debtors</b></div><br>
              @forelse ($products as $product)
              <div class="col-md-3">{{ $product->item_name }}</div>
              <div class="col-md-3">{{ $product->total }}</div>
              @empty
              <div class="col-md-6">No Transaction Held To This Date Range</div>
              @endforelse
            </td>
          </tr>

          <tr>
            <td><div class="col-md-3"><b>Payment Type</b></div><br>
              @forelse ($payment as $paymnt)
              <div class="col-md-3">{{ $paymnt->getPaymentType->payment_type }}</div>
              <div class="col-md-3">{{ $paymnt->amount }}</div>
              <div class="col-md-3"></div>
              <div class="col-md-3">{{ $paymnt->amount }}</div>
              @empty
              <div class="col-md-6">No Payment Here This Date Range</div>
              @endforelse
            </td>
          </tr>

        </tbody>
        

        {{-- <thead>
          <tr>
            <th class="">
              <div class="col-md-6">Total:</div>
              <div class="col-md-6">@if(!empty($pay)){{ $pay->amount }} @endif</div>
            </th>
            <th class="">
              <div class="col-md-6" >Total:</div>
              <div class="col-md-6" >@if( !empty($recev)){{ $recev->quantity }} @endif</div>
            </th>
          </tr>
        </thead> --}}
      </table>
        <div  class="col-sm-5 col-md-offset-4">
         {{-- <p class="m-n"><b>{{ $set->getCompany->copyright }}</b></p> --}}
       </div>
     
    </div>
  </div>
</div>

@endsection
