@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
          <a target="_blank" href="" class="btn btn-primary btn-sm"><i
              class="fa fa-folder-open"  ></i>&nbsp; Print Into PDF
          </a>
        </div>
     </div>
   </div>
    <div class="panel-body page-break">
      <div class="row align-items-center">
        <div class="col-sm-5 col-md-offset-4" style="text-align:center;">
          <div style="text-align:center;">
          <img style="margin-left: 97px;" src="{{asset('public/CompLogo/'.$set->getCompany->comp_logo)}}" class="img-responsive" />
          <h4 class="m-n">{{ $set->getCompany->company }}</h4>
          <h4 class="m-n">{{ $set->getCompany->address }}</h4>
          <h2 class="m-n">Balance Sheet</h2>
          <p class="m-n">{{ date('d-m-Y', strtotime($from)) }}</p>
          <br>
        </div>
     </div>

     <div class="panel-body">
      <table id="pattern-type-data" class="table table-bordered ">
        <thead style="background-color:#b3ecff"  >
          <tr>
            <th>
              <div class="col-md-6">Particulars</div>
              <div class="col-md-3">Amount</div>
              <div class="col-md-3">Amount</div>
            </th>
        </tr>
        </thead>
        <tbody>
          {{-- @forelse ($banks as $bank)
          <tr>
            <td>{{ $bank->bank_name }}</td>
            <td>{{ $bank->initial_amount }}</td>
            @empty
            <td>No Bank Transaction Here This Date Range</td>
          </tr> 
          @endforelse --}}
          <tr>
            <td><div class="col-md-3"><b>Current Assets</b></div><br>
              <div class="col-md-6">
                &nbsp;&nbsp;&nbsp;&nbsp;Bank Account <br>
                &nbsp;&nbsp;&nbsp;&nbsp;Cash in Hand <br>
                &nbsp;&nbsp;&nbsp;&nbsp;Sundry Debtors
              </div>
              <div class="col-md-3">
                {{ $banks->initial_amount }} <br>
                {{ $banks->initial_amount }} <br>
                {{ $banks->initial_amount }} <br>
              </div>
              <div class="col-md-3">&nbsp;&nbsp;&nbsp;&nbsp;<b>3000</b></div>
              
            </td>
          </tr>
          <tr>
            <td><div class="col-md-3"><b>Fixed Assets</b></div><br>
              <div class="col-md-6">
                &nbsp;&nbsp;&nbsp;&nbsp;Office Eqipment <br>
                &nbsp;&nbsp;&nbsp;&nbsp;Electrical Equipments <br>
                &nbsp;&nbsp;&nbsp;&nbsp;Plant
              </div>
              <div class="col-md-3">
                {{ $banks->initial_amount }} <br>
                {{ $banks->initial_amount }} <br>
                {{ $banks->initial_amount }} <br>
              </div>
              <div class="col-md-3">&nbsp;&nbsp;&nbsp;&nbsp;<b>3000</b></div>
              
            </td>
          </tr>
        </tbody>
      </table>
        <div  class="col-sm-5 col-md-offset-4">
         {{-- <p class="m-n"><b>{{ $set->getCompany->copyright }}</b></p> --}}
       </div>
     
    </div>
  </div>
</div>

@endsection
