@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
          <p class="m-n font-thin h3 text-primary"><i class="fa fa-credit-card"></i> &nbspAll Report</p>
        </div>
        <div class="col-md-2">
        </div>
     </div>
   </div>
    <div class="panel-body col-md-offset-1">
      <div style="" class="row">
        <div class="col-md-6">
          <table class="table table-hover">
            <tbody>
              <tr>
                <td><h4><span><i class="fa fa-database" aria-hidden="true"></i></span>&nbsp; <a href=" {{ route('receive_paym') }} ">Receive And Payment</a></h4></td>
              </tr>
              {{-- <tr>
                <td><h4><span><i class="fa fa-tasks" aria-hidden="true"></i></span>&nbsp; <a href="">Profit and Loss</a></h4></td>
              </tr> --}}
              {{-- <tr>
                <td><h4><span><i class="fa fa-tasks" aria-hidden="true"></i></span>&nbsp; <a href="">Cash Book (Summary)</a></h4></td>
              </tr> --}}
              <tr>
              <td><h4><span><i class="fa fa-tasks" aria-hidden="true"></i></span>&nbsp; <a href="{{ route('day_book_index') }}">Day Book ( Summary )</a></h4></td>
              </tr>
              <tr>
                <td><h4><span><i class="fa fa-bookmark" aria-hidden="true"></i></span>&nbsp; <a href=" {{ route('balance') }} ">Balance Sheet</a></h4></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-6">
          <table class="table table-hover">
            <tbody>
              <tr>
              <td><h4><span><i class="fa fa-bank" aria-hidden="true"></i></span>&nbsp; <a href="{{ route('bank_book') }}">Bank Book (Detail)</a></h4></td>
              </tr>
              <tr>
                <td><h4><span><i class="fa fa-bar-chart" aria-hidden="true"></i></span>&nbsp; <a href="{{ route('group_index') }}">Group Summary</a></h4></td>
              </tr>
              {{-- <tr>
                <td><h4><span><i class="fa fa-tachometer" aria-hidden="true"></i></span>&nbsp; <a href="">Trial Balance</a></h4></td>
              </tr> --}}
              {{-- <tr>
                <td><h4><span><i class="fa fa-tachometer" aria-hidden="true"></i></span>&nbsp; <a href="">Cash & Bank Multi Ledger</a></h4></td>
              </tr> --}}
              <tr>
                <td><h4><span><i class="fa fa-line-chart" aria-hidden="true"></i></span>&nbsp; <a href="">Bank Reconciliation</a></h4></td>
              </tr>
              {{-- <tr>
                <td><h4><span><i class="fa fa-suitcase" aria-hidden="true"></i></span>&nbsp; <a href="">Transaction Details</a></h4></td>
              </tr> --}}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
