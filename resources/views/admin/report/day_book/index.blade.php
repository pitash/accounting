@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
          <p class="m-n font-thin h3 text-primary"><i class="fa fa-tasks"></i> &nbspDay Book</p>
        </div>
        <div class="col-md-2">
        </div>
     </div>
   </div>
    <div class="panel-body">
    <form class="form-horizontal" action="{{ route('day_book_post') }}" method="post" enctype="multipart/form-data">
        @csrf
       <div class="row">
         <div class="col-md-12">
           <br>
           <div class="col-md-4">
             <div class="form-group">
               <label class="col-md-4 control-label">From &nbsp<span style="color:red;">*</span></label>
               <div class="col-md-8 inputGroupContainer">
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  <input name="from" class="form-control" required="true" value="{{ old('from') }}" type="date"></div>
             </div>
           </div>
         </div>
         <div class="col-md-4">
           <div class="form-group">
             <label class="col-md-4 control-label">To &nbsp<span style="color:red;">*</span></label>
             <div class="col-md-8 inputGroupContainer">
              <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input name="to" class="form-control" required="true" value="{{ old('to') }}" type="date"></div>
           </div>
           </div>
         </div>
           <div class="col-md-3">
              <button type="submit"  class="btn btn-primary center-block fa fa-plus-square">&nbsp Generate</button>
           </div>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

@endsection
