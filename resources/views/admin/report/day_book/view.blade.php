@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
          <a target="_blank" href="" class="btn btn-primary btn-sm"><i
              class="fa fa-folder-open"  ></i>&nbsp; Print Into PDF
          </a>
        </div>
     </div>
   </div>
    <div class="panel-body page-break">
      <div class="row align-items-center">
        <div class="col-sm-5 col-md-offset-4" style="text-align:center;">
          <div style="text-align:center;">
          <img style="margin-left: 97px;" src="{{asset('public/CompLogo/'.$set->getCompany->comp_logo)}}" class="img-responsive" />
          <h4 class="m-n">{{ $set->getCompany->company }}</h4>
          <h4 class="m-n">{{ $set->getCompany->address }}</h4>
          <h2 class="m-n">Day Book Summary</h2>
          <p class="m-n">{{ date('d-m-Y', strtotime($from)) }} to {{ date('d-m-Y', strtotime($to)) }}</p>
          <br>
        </div>
     </div>

     <div class="panel-body">
      <table id="pattern-type-data" class="table">
        <thead style="background-color:#b3ecff"  >
          <tr>
            <th class="">
              <div class="col-md-2">Date</div>
              <div class="col-md-3">Vch. No</div>
              <div class="col-md-3">Particulars</div>
              <div class="col-md-2">Debit</div>
              <div class="col-md-2">Credit</div>
            </th>
        </tr>
        </thead>
        <tbody>

          <tr>
            <td><div class="col-md-3"><b>Payment</b></div><br>
              @forelse ($payments as $payment)
              <div class="col-md-2">{{ $payment->date }}</div>
              <div class="col-md-3">{{ $payment->voucher_no }}</div>
              <div class="col-md-3">{{ $payment->getSupp->supplier }}</div>
              <div class="col-md-2">{{ $payment->amount }}</div>
              <div class="col-md-2">&nbsp;</div>
              @empty
              <div class="col-md-6">No Payment Here This Date Range</div>
              @endforelse
            </td>
          </tr>

          <tr>
            <td><div class="col-md-3"><b>Receipt</b></div><br>
              @forelse ($receives as $receive)
              <div class="col-md-2">{{ $receive->bill_date }}</div>
              <div class="col-md-3">{{ $receive->order_number }}</div>
              <div class="col-md-3">{{ $receive->getSupp->supplier }}</div>
              <div class="col-md-2"></div>
              <div class="col-md-2">{{ $receive->total }}</div>
              @empty
              <div class="col-md-6">No ReceiveTo This Date Range</div>
              @endforelse
            </td>
          </tr>

          <tr>
            <td><div class="col-md-3"><b>Purchase</b></div><br>
              @forelse ($purchases as $purchase)
              <div class="col-md-2">{{ $purchase->date }}</div>
              <div class="col-md-3">{{ $purchase->purchase_no }}</div>
              <div class="col-md-3">{{ $purchase->getParty->supplier }}</div>
              <div class="col-md-2"></div>
              <div class="col-md-2">{{ $purchase->total }}</div>
              @empty
              <div class="col-md-6">No Purchase Here This Date Range</div>
              @endforelse
            </td>
          </tr>

        </tbody>
        

        {{-- <thead>
          <tr>
            <th class="">
              <div class="col-md-6">Total:</div>
              <div class="col-md-6">@if(!empty($pay)){{ $pay->amount }} @endif</div>
            </th>
            <th class="">
              <div class="col-md-6" >Total:</div>
              <div class="col-md-6" >@if( !empty($recev)){{ $recev->quantity }} @endif</div>
            </th>
          </tr>
        </thead> --}}
      </table>
        <div  class="col-sm-5 col-md-offset-4">
         {{-- <p class="m-n"><b>{{ $set->getCompany->copyright }}</b></p> --}}
       </div>
     
    </div>
  </div>
</div>

@endsection
