@extends('admin.pages.dashboard')
 @section('content')
<style>
  table tr th{
    background-color: #dee5e7;
    color: #fff;
  }
</style>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
          <a target="_blank" href="" class="btn btn-primary btn-sm"><i
              class="fa fa-folder-open"  ></i>&nbsp; Print Into PDF
          </a>
        </div>
     </div>
   </div>
    <div class="panel-body page-break">
      <div class="row align-items-center">
        <div class="col-sm-5 col-md-offset-4" style="text-align:center;">
          <div style="text-align:center;">
          <img style="margin-left: 97px;" src="{{asset('public/CompLogo/'.$set->getCompany->comp_logo)}}" class="img-responsive" />
          <h4 class="m-n">{{ $set->getCompany->company }}</h4>
          <h4 class="m-n">{{ $set->getCompany->address }}</h4>
          <h2 class="m-n">Bank Book</h2>
          <p class="m-n">{{ date('d-m-Y', strtotime($from)) }} to {{ date('d-m-Y', strtotime($to)) }}</p>
          <br>
        </div>
     </div>

     <div class="panel-body">
        <div class="col-md-10 col-md-offset-1" >
          <table class="table table-bordered text-center">
            <tr>
              <th class="text-center">Created At</th>
              <th class="text-center">Bank Name</th>
              <th class="text-center">Initial Balance</th>
            </tr>
            @forelse ($banks as $bank)
            <tr>
              <td>{{ date('d-m-Y', strtotime($bank->created_at)) }}</td>
              <td>{{ $bank->bank_name }}</td>
              <td>{{ $bank->initial_amount }}</td>
            </tr>
            @empty
            <tr>
              <td colspan="3" style="text-align:center;" ><h3>No Data Found This Date Range</h3></td>
            </tr>
            @endforelse
          </table>
          <div  class="col-sm-12 col-md-offset-5">
           {{-- <p class="m-n"><b>{{ $set->getCompany->copyright }}</b></p> --}}
         </div>
        </div>
     
    </div>
  </div>
</div>

@endsection
