@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
          <a target="_blank" href="" class="btn btn-primary btn-sm"><i
              class="fa fa-folder-open"  ></i>&nbsp; Print Into PDF
          </a>
        </div>
     </div>
   </div>
    <div class="panel-body page-break">
      <div class="row align-items-center">
        <div class="col-sm-5 col-md-offset-4" style="text-align:center;">
          <div style="text-align:center;">
          <img style="margin-left: 97px;" src="{{asset('public/CompLogo/'.$set->getCompany->comp_logo)}}" class="img-responsive" />
          <h4 class="m-n">{{ $set->getCompany->company }}</h4>
          <h4 class="m-n">{{ $set->getCompany->address }}</h4>
          <h2 class="m-n">Receive & Payment</h2>
          <p class="m-n">{{ date('d-m-Y', strtotime($from)) }} to {{ date('d-m-Y', strtotime($to)) }}</p>
          <br>
        </div>
     </div>

     <div class="panel-body">
      <table id="pattern-type-data" class="table table-bordered ">
        <thead style="background-color:#b3ecff"  >
          <tr>
          <th class="">
            <div class="col-md-6">Payment</div>
            <div class="col-md-6">Amount</div>
          </th>
          <th class="">
            <div class="col-md-6">Receive</div>
            <div class="col-md-6">Amount</div>
          </th>
        </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              @forelse ($payment as $pay)
              <div class="col-md-6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $pay->getSupp->supplier }}</div>
              <div class="col-md-6">{{ $pay->amount }}</div>
              @empty
              <div class="col-md-6">No Payment Here This Date Range</div>
              @endforelse
            </td>
            <td>
              @forelse ($receive as $recev)
              <div class="col-md-6">{{ $recev->desc }}</div>
              <div class="col-md-6">{{ $recev->quantity*$recev->rate }} </div>
              @empty
              <div class="col-md-6">No Receive Here This Date Range</div>
              @endforelse
            </td>
          </tr> 
        </tbody>
        {{-- <thead>
          <tr>
            <th class="">
              <div class="col-md-6">Total:</div>
              <div class="col-md-6">@if(!empty($pay)){{ $pay->amount }} @endif</div>
            </th>
            <th class="">
              <div class="col-md-6" >Total:</div>
              <div class="col-md-6" >@if( !empty($recev)){{ $recev->quantity }} @endif</div>
            </th>
          </tr>
        </thead> --}}
      </table>
        <div  class="col-sm-5 col-md-offset-4">
         {{-- <p class="m-n"><b>{{ $set->getCompany->copyright }}</b></p> --}}
       </div>
     
    </div>
  </div>
</div>

@endsection
