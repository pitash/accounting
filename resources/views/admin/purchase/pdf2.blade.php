<!DOCTYPE html>
<html>
  <head>
    <title>Purchase Order</title>
    <style>
    div.b {
      line-height: 1.6;
      text-align: center;
    }

  .divLeft {
      width:500px;
      display:block;
      float: left;
    }
    .divRight {
      width:600px;
      display:block;
      float: right;
      padding-left: 450px;
    }

    table, td, th {
      border: 1px solid black;
      text-align: center;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th {
      height: 50px;
      /* text-align: center; */
    }
    .footer {
        position: fixed;
        text-align: center;
        left: 0;
        bottom: 0;
        width: 100%; 
      }
      
    .split {
    float: left;
    margin-right:220px;
    margin-bottom: 0px;
    }

      .row{
        padding-left: 45px;
    }

    </style>
  </head>
<body>

  <div class="b">
    <img src="{{ asset('public/CompLogo/'.$setting->getCompany->comp_logo) }}" class="img-responsive" /><br>
    {{ $setting->getCompany->company }} <br>
    {{ $setting->getCompany->address }}
  </div>

  <h3 style="text-align: center;" >Purchase Order</h3><br><br>
  
  <div class="divLeft">
   <p>Purchase No #: {{ $purchase->purchase_no }}</p>
   <p>Party Name: <b>{{ $purchase->getParty->supplier }}</b></p>
  </div>

  <div class="divRight">
    <p>Date : {{ $purchase->date }}</p>
    <p>Reference NO : {{ $purchase->reference_no }}</p>
  </div><br><br><br><br><br><br><br>
  
  <table>
    <tr>
      <th>SL #</th>
      <th>Name of the Item</th>
      <th>Quantity</th>
      <th>Rate</th>
      <th>Total</th>
    </tr>
    @foreach ($purchase->getProduct as $item)
    <tr>
      <td>{{ $sl++ }}</td>
      <td>{{ $item->item_name }}</td>
      <td>{{ $item->quantity }}</td>
      <td>{{ $item->rate }}</td>
      <td>{{ $item->total }}</td>
    </tr>
    @endforeach
    <tr>
      <td colspan="2" >Total :</td>
    <td colspan="" style="" >{{ $quant }} </td>
      <td colspan="2" style="padding-left: 179px;" >{{ $total}}</td>
    </tr>
  </table>
  <p><b>Note :</b> {{ $purchase->note }}</p><br><br><br><br><br><br><br><br><br>
  <div class="row abc">
      <div class="split">
          <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Customer' }} </span></label>
      </div>
      <div class="split">
          
      </div>
      <div class="cols">
          <div class="split">
              <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Authorised' }} </span></label>
          </div>
          <div class="split">
              
          </div>
      </div>
    </div>

    <div class="footer">
      <h1></h1>
      <p>{{ $setting->getCompany->copyright }}</p>
    </div>
  </body>
</html>