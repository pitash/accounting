<!DOCTYPE html>
<html>
  <head>
    <title>Purchase Order</title>
    <style>

      div.b {
            line-height: 1.6;
            text-align: center;
          }

        .divLeft {
            width:500px;
            display:block;
            float: left;
          }
          .divRight {
            width:600px;
            display:block;
            float: right;
            padding-left: 450px;
          }
      table, td, th {  
        border: 1px solid #ddd;
        text-align: left;
      }
      th {  
        background-color:#b3ecff;
      }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      th, td {
        padding: 10px;
      }

      .split {
      float: left;
      margin-right:220px;
      margin-bottom: 0px;
      }

      .row{
        padding-left: 45px;
      }


      .footer {
        position: fixed;
        text-align: center;
        left: 0;
        bottom: 0;
        width: 100%; 
      }

    </style>
  </head>
<body>

  <div class="b">
    <img src="{{ asset('public/CompLogo/'.$setting->getCompany->comp_logo) }}" class="img-responsive" /><br>
    {{ $setting->getCompany->company }} <br>
    {{ $setting->getCompany->address }}
  </div>

  <h3 style="text-align: center;" >Purchase Order</h3><br><br>
  
  <div class="divLeft">
   <p><b>Purchase No #</b>: {{ $purchase->purchase_no }}</p>
   <p><b>Party Name:</b> {{ $purchase->getParty->supplier }}</p>
   <p><b>Bank Account:</b> {{ $purchase->getBank->bank_name }}</p>
  </div>

  <div class="divRight">
    <p><b>Date :</b> {{ $purchase->date }}</p>
    <p><b>Reference NO :</b> {{ $purchase->reference_no }}</p>
    <p><b>Under :</b> {{ $purchase->getUnder->title }}</p>
  </div><br><br><br><br><br><br><br><br>
  <table>
    <tr>
      <th>SL</th>
      <th>Name of the Item</th>
      <th>Quantity</th>
      <th>Rate</th>
      <th>Amount</th>
    </tr>
    {{-- @foreach ($purchase as $item) --}}
    <tr>
      <td>{{ $sl++ }}</td>
      <td>{{ $purchase->item_name }}</td>
      <td>{{ $purchase->quantity }}</td>
      <td>{{ $purchase->rate }}</td>
      <td>{{ $purchase->total }}</td>
    </tr>
    {{-- @endforeach --}}
    <tr>
      <td colspan="2" ><i>Total :</i></td>
    <td colspan="" style="" >{{ $purchase->quantity }} </td>
      <td colspan="2" style="padding-left: 179px;"><b>{{ $purchase->total }}</b></td>
    </tr>
  </table>
  <p><b>Description :</b> {{ $purchase->note }}</p><br><br><br><br><br><br><br><br><br>
  
  <div class="row abc">
    <div class="split">
        <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Supplier' }} </span></label>
    </div>

    <div class="split">
        
    </div>

    <div class="cols">
        <div class="split">
            <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Authorised' }} </span></label>
        </div>
        <div class="split">
            
        </div>
    </div>
  </div>

  <div class="footer">
    <h1></h1>
    <p>{{ $setting->getCompany->copyright }}</p>
  </div>
</body>
</html>
