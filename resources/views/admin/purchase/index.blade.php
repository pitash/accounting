@extends('admin.pages.dashboard')
 @section('content')
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
          <p class="m-n font-thin h3 text-primary"><i class="fa fa-credit-card"></i> &nbspPurchase Order List</p>
        </div>
        <div class="col-md-2">
          <a style="font-size: 14px;" href="{{ route('purchase.create') }}" class="btn btn-primary btn-sm"><i
              class="fa fa-plus"></i>&nbsp;Add New Voucher</a>
        </div>
     </div>
   </div>
    <div class="panel-body">
      <table id="pattern-type-data" class="table table-bordered text-center">
        <thead>
        <tr>
          <th class="text-center">SL.</th>
          <th class="text-center">Pur No</th>
          <th class="text-center">Refer No</th>
          <th class="text-center">Party Acc</th>
          <th class="text-center">Bank Ac</th>
          <th class="text-center">Under</th>
          <th class="text-center">Description</th>
          {{-- <th class="text-center">Quantity</th>
          <th class="text-center">Price (Single)</th> --}}
          {{-- <th class="text-center">Total</th> --}}
          <th class="text-center">Date</th>
          <th class="text-center">Created By</th>
          <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
          @forelse ($purches as $purch)
          <tr>
            <td>{{ $sl++ }}</td>
            <td style="width:10px;">{{ $purch->purchase_no }}</td>
            <td style="width:10px;">{{ $purch->reference_no }}</td>
            <td>{{ $purch->getParty->supplier }}</td>
            <td>{{ $purch->getBank->bank_name }}</td>
            <td>{{ $purch->getUnder->title }}</td>
            <td style="width:240px;">{{ $purch->note }}</td>
            <td>{{ $purch->date }}</td>
            <td style="width:10px;">{{ $purch->getUser->name }}</td>
            <td style="width:140px;">
              <form action="{{ route('purchase.destroy',$purch->id) }}" method="POST">
                <a href="{{ route('purchase.show',$purch->id) }}" data-toggle="tooltip" data-placement="top" title='Voucher Image' class=" btn btn-sm btn-info" target="_blank"><i class="fa fa-film" aria-hidden="true"></i></a>
                <button type="button" data-toggle="tooltip" data-placement="top" title='Edit' class=" btn btn-sm btn-success edit_link" value="{{ route('purchase.edit',$purch->id) }}" ><i class="fa fa-pencil"> </i></button>
                <a href="{{ url('purch-pdf') }}/{{ $purch->id }} " data-toggle="tooltip" data-placement="top" title='Make PDF' class=" btn btn-sm btn-warning" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                @csrf
                @method('DELETE')
                {{-- <button type="submit" data-toggle="tooltip" data-placement="top" title='Delete' class=" btn btn-sm btn-success" onclick="return confirm('Are you sure you want to Delete ?');" ><i class="fa fa-trash"> </i></button> --}}
              </form>
            </td>
          </tr>
          @empty
          <tr>
          <td colspan="12" class="text-center"><h2>No Purches Here</h2></td>
        </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection
