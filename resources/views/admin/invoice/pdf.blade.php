<!DOCTYPE html>
<html>
  <head>
    <title>Invoice</title>
    <style>

      div.b {
            line-height: 1.6;
            text-align: center;
          }

        .divLeft {
            width:500px;
            display:block;
            float: left;
          }
          .divRight {
            width:600px;
            display:block;
            float: right;
            padding-left: 450px;
          }
      table, td, th {  
        border: 1px solid #ddd;
        text-align: left;
      }
      th {  
        background-color:#b3ecff;
      }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      th, td {
        padding: 10px;
      }

      .split {
        float: left;
        margin-right:90px;
        margin-bottom: 0px;
        }

      .row{
        padding-left: 45px;
      }


      .footer {
        position: fixed;
        text-align: center;
        left: 0;
        bottom: 0;
        width: 100%; 
      }

    </style>
  </head>
<body>

  <div class="b">
    <img src="{{ asset('public/CompLogo/'.$setting->getCompany->comp_logo) }}" class="img-responsive" /><br>
    {{ $setting->getCompany->company }} <br>
    {{ $setting->getCompany->address }}
  </div>

  <h3 style="text-align: center;" >Invoice Slip</h3><br><br>
  
  <div class="divLeft">
   <p><b>Serial No. #</b>: {{ $invoice->invoice_no }}</p>
   <p><b>Client Name:</b> {{ $invoice->getCust->getCust->title }}</p>
   <p><b>Bill Date:</b> {{ $invoice->date }}</p>
  </div>

  <div class="divRight">
    <p><b>Payment Method:</b> {{ $invoice->pay_method }}</p>
    <p><b>Status :</b> {{ $invoice->status }}</p>
    <p><b>Due Date :</b> {{ $invoice->due_date }}</p>
  </div><br><br><br><br><br><br><br><br>
  <table>
    <tr>
        <th>Item</th>
        {{-- <th>Description</th> --}}
        <th>Rate</th>
        <th>Quantity</th>
        <th>Price</th>
    </tr>
    <tr>
      <td style="width:200px;">{{ $invoice->item_name }}</td>
        {{-- <td>{{ $invoice->desc }}</td> --}}
        <td>{{ $invoice->price }}</td>
        <td>{{ $invoice->quantity }}</td>
        <td>{{ $invoice->total }}</td>
    </tr>
    <tr>
      <td colspan="1" ><i>Total :</i></td>
      {{-- <td colspan="" style="" >{{ $invoice->quantity }} </td> --}}
      <td colspan="3" style="padding-left: 400px;"><b>{{ $invoice->total }}</b></td>
    </tr>
  </table>
  <p><b>Description :</b> {{ $invoice->desc }}</p><br><br><br><br><br><br><br><br><br>
  
    <div class="row abc">
      <div class="split">
          <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Prepared By' }} </span></label>
      </div>
      <div class="split">
        <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Checked By' }} </span></label>
      </div>
      <div class="cols">
          <div class="split">
              <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Recomended By' }} </span></label>
          </div>
          <div class="split">
            <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Approved By' }} </span></label>
          </div>
      </div>
    </div>

  <div class="footer">
    <h1></h1>
    <p>{{ $setting->getCompany->copyright }}</p>
  </div>
</body>
</html>
