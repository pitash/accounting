@extends('admin.pages.dashboard')
 @section('content')
 <style type="text/css">
  .fa{
    font-size: 14px;
  }
 </style>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-md-10">
          <p class="m-n font-thin h3">All Payment Type</p>
        </div>
        <div class="col-md-2">
          <a href="{{ route('payment_type.create') }}" class="btn btn-primary btn-sm"><i
              class="fa fa-plus"></i>&nbsp;Add New Type</a>
        </div>
     </div>
   </div>
    <div class="panel-body">
      <table id="pattern-type-data" class="table table-bordered table text-center">
        <thead>
        <tr>
            <th class="text-center">SL</th>
            <th class="text-center">Payment Type</th>
            <th class="text-center">Status</th>
            <th class="text-center">Actions</th>
            <th class="text-center">Created By</th>
        </tr>
        </thead>
        <tbody>
          <?php $a=1; ?>
          @forelse ($ptyps as $ptyp)
          <tr>
            <td>{{$a++}}</td>
            <td>{{ $ptyp->payment_type }}</td>
            <td>{{ ($ptyp->status == 1) ? ' Active' : ' Deactive' }}</td>
            <td>
             <form action="{{ route('payment_type.destroy',$ptyp->id) }}"method="POST">

               <button type="button" data-toggle="tooltip" data-placement="top" title='Edit' class=" btn btn-sm btn-info edit_link"
               value="{{ route('payment_type.edit',$ptyp->id) }}" ><i class="fa fa-pencil"> </i></button>
               
               <a type="button" class="btn-sm {{ ($ptyp->status == 1) ? 'fa fa-toggle-off btn-success': '	fa fa-toggle-on btn-warning'}}" href=
                "{{ route('payment_type.show',$ptyp->id) }}"></a>
                 
                @csrf
                 @method('DELETE')
                 {{-- <button type="submit" data-toggle="tooltip"
                 data-placement="top" title='Delete' class=" btn btn-sm btn-success" onclick="return confirm('Are you sure you want to delete this Type ?');" ><i class="fa fa-trash"> </i></button> --}}
             </form>
            </td>
            <td>{{ $ptyp->getUser->name }}</td>
          </tr>
          @empty
            <tr>
            <td colspan="5" class="text-center"><h2>There Is No Payment Type</h2></td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
