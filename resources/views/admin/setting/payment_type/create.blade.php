@extends('admin.pages.dashboard')
@section('content')
<div class="wrapper-md">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        <div class="row">
          <div class="col-md-10">
            <p class="m-n font-thin h3">Create Payment Type </p>
          </div>
          <div class="col-md-2">
            <a href="{{route('payment_type.index')}}" class="btn btn-info btn-sm">
                <i class="fa fa-list-ul"></i>&nbsp;&nbsp;All Type</a>
          </div>
        </div>
      </div>

      <div class="panel-body">
        @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form class="well form-horizontal" action="{{ route('payment_type.store') }}" method="post" enctype="multipart/form-data">
            @csrf
           <div class="row">
             <div class="col-sm-5 col-md-offset-3">
               <legend></legend>

               <div class="form-group">
                 <label class="col-md-4 control-label">{{ 'Payment Type' }} <span style="color:red;">*</span></label>
                  <div class="col-md-8 inputGroupContainer">
                     <div class="input-group"><span class="input-group-addon"><i class="fa fa-google-wallet"></i></span>
                       <input id="fullName" name="payment_type" placeholder="Payment Type" class="form-control" required="true" value="" type="text"></div>
                  </div>
               </div>
             </div>
           </div>
           <br>
            <button type="submit" class="btn btn-primary center-block">Submit</button>
          </form>
      </div>
    </div>
  </div>
</div>
@endsection
