<!DOCTYPE html>
<html>
  <head>
    <title>Payment Order</title>
    <style>

      div.b {
            line-height: 1.6;
            text-align: center;
          }

        .divLeft {
            width:500px;
            display:block;
            float: left;
          }
          .divRight {
            width:600px;
            display:block;
            float: right;
            padding-left: 450px;
          }
      table, td, th {  
        border: 1px solid #ddd;
        text-align: left;
      }
      th {  
        background-color:#b3ecff;
      }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      th, td {
        padding: 10px;
      }

      .split {
        float: left;
        margin-right:90px;
        margin-bottom: 0px;
        }

      .row{
        padding-left: 45px;
      }


      .footer {
        position: fixed;
        text-align: center;
        left: 0;
        bottom: 0;
        width: 100%; 
      }

    </style>
  </head>
<body>

  <div class="b">
    <img src="{{ asset('public/CompLogo/'.$setting->getCompany->comp_logo) }}" class="img-responsive" /><br>
    {{ $setting->getCompany->company }} <br>
    {{ $setting->getCompany->address }}
  </div>

  <h3 style="text-align: center;" >Payment Order</h3><br><br>
  
  <div class="divLeft">
   <p><b>Voucher No #</b>: {{ $payment->voucher_no }}</p>
   <p><b>From Account:</b> {{ $payment->getSupp->supplier }}</p>
   <p><b>Payment Method:</b> {{ $payment->getPaymentType->payment_type }}</p>
  </div>

  <div class="divRight">
    <p><b>Date :</b> {{ $payment->date }}</p>
    <p><b>Reference No :</b> {{ $payment->reference }}</p>
    {{-- <p><b>Under :</b> {{ $purchase->getUnder->title }}</p> --}}
  </div><br><br><br><br><br><br><br><br>
  <table>
    <tr>
      <th>Particulars</th>
      <th>Amount</th>
      {{-- <th>Rate</th> --}}
      <th>Total</th>
    </tr>
    <tr>
      <td>{{ $payment->getCust->getCust->title }}</td>
      <td>{{ $payment->amount }}</td>
      <td>{{ $payment->amount }}</td>
    </tr>
  </table>
  <p><b>Description :</b> {{ $payment->desc }}</p><br><br><br><br><br><br><br><br><br>
  
    <div class="row abc">
      <div class="split">
          <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Prepared By' }} </span></label>
      </div>
      <div class="split">
        <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Checked By' }} </span></label>
      </div>
      <div class="cols">
          <div class="split">
              <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Recomended By' }} </span></label>
          </div>
          <div class="split">
            <label style="" class="col-md-4 control-label">----------------<br><span class="sp">{{ 'Approved By' }} </span></label>
          </div>
      </div>
    </div>

  <div class="footer">
    <h1></h1>
    <p>{{ $setting->getCompany->copyright }}</p>
  </div>
</body>
</html>
