<?php

namespace App\Http\Controllers;

use App\Payment;
use App\ReceiveVoucher;
use App\Setting;
use Illuminate\Http\Request;

class ReceivePaymentController extends Controller
{
    public function index()
    {
      return view('admin.report.receive_payment.index');
    }

    public function store(Request $request)
    {
      $from = $request->get('from');
      $to = $request->get('to');
      $request->session()->put('from',$from);
      $request->session()->put('to',$to);
      $payment = Payment::whereBetween('date',[$from, $to])->get();
      $receive = ReceiveVoucher::whereBetween('bill_date',[$from, $to])->get();
      $set = Setting::first();
      return view('admin.report.receive_payment.stock', compact('payment','receive','set','from','to'));
    } 
}
