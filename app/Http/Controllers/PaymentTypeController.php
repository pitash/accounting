<?php

namespace App\Http\Controllers;

use App\PaymentType;
use Auth;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ptyps = PaymentType::all();
        $sl = 1;
        return view('admin.setting.payment_type.index',compact('ptyps','sl') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.setting.payment_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request-> validate([
        'payment_type' => 'required|unique:payment_types,payment_type',
      ]);

      PaymentType::insert([
       'payment_type' => $request->payment_type,
       'created_by' => Auth::id(),
       'created_at' => Carbon::now(),
     ]);

     return redirect('payment_type')->with('success','Payment Type Add Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function show($ptyp_id)
    {
        $status_info = PaymentType::find($ptyp_id);
        if($status_info->status == 1){
          $status_info->status = 2;
        }
        else {
          $status_info->status = 1;
        }
        $status_info->save();
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function edit($ptyp_id)
    {
      $ptyp = PaymentType::findOrFail($ptyp_id);
      return view('admin.setting.payment_type.edit', compact('ptyp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ptyp_id)
    {
      $request->validate([
        'payment_type'=> 'required',
      ]);

      PaymentType::find($ptyp_id)->update($request->all());
      return Redirect::to('payment_type')->with('success', 'Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function destroy($ptyp_id)
    {
      ayType::where('id', "=", $ptyp_id)->delete();
      return Redirect::to('paytype')->with('success', 'Delete Successfully!!');
    }
}
