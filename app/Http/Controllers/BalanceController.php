<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Setting;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
  public function index()
  {
    return view('admin.report.balance.index');
  }

  public function store(Request $request)
  {
    $from = $request->get('from');
      // $to = $request->get('to');
      $request->session()->put('from',$from);
      // $request->session()->put('to',$to);
      $set = Setting::first();
      $banks = Bank::first();
      return view('admin.report.balance.view', compact('set','from','banks'));
  }
}
