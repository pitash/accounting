<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Payment;
use App\Purchase;
use App\ReceiveVoucher;
use Illuminate\Http\Request;

class DayBookController extends Controller
{
    public function index()
    {
      return view('admin.report.day_book.index');
    }

    public function store(Request $request)
    {
      $from = $request->get('from');
      $to = $request->get('to');
      $request->session()->put('from',$from);
      $request->session()->put('to',$to);
      $set = Setting::first();
      $payments = Payment::whereBetween('date',[$from, $to])->get();
      $receives = ReceiveVoucher::whereBetween('bill_date',[$from, $to])->get();
      $purchases = Purchase::whereBetween('date',[$from, $to])->get();
      return view('admin.report.day_book.view', compact('from','to','set','payments','receives','purchases'));
    }
}
