<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Payment;
use App\PaymentType;
use App\Product;
use App\Setting;
use App\Supplier;
use Illuminate\Http\Request;

class GroupSummaryController extends Controller
{
    public function index()
    {
     return view('admin.report.group_summary.index');
    }

    public function groupPost(Request $request)
    {
      $from = $request->get('from');
      $to = $request->get('to');
      $request->session()->put('from',$from);
      $request->session()->put('to',$to);
      $set = Setting::first();
      $banks = Bank::whereBetween('created_at',[$from, $to])->get();
      $products = Product::whereBetween('created_at',[$from, $to])->get();
      $payment = Payment::whereBetween('date',[$from, $to])->get();
      return view('admin.report.group_summary.view', compact('banks','set','from','to','products','payment'));
    }
}
