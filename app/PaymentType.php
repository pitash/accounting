<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
  protected $fillable = ['payment_type'];

  function getUser()
  {
    return $this->hasOne('App\User','id','created_by');
  }
}
