<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = ['item_name','rate','quantity','total','purch_id','created_by'];

  function getUser()
    {
      return $this->hasOne('App\User','id','created_by');
    }

  function getProduct()
    {
      return $this->hasMany('App\Purchase','purch_id');
    } 
}
